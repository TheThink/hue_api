<?php
/**
 * Copyright: 2016 - David Rühr <csw.ruehr@googlemail.com>
 * Hue Color Light
 */
require_once $baseDir . 'Light.php';

/**
 * Class ColorLight
 */
class ColorLight extends Light {
	const ALERT_NONE = 'NONE';
	const ALERT_ONE_BLINK = 'select';
	const ALERT_BLINKING = 'lselect';
	const ALERT_TIME_SHORT = 0;
	const ALERT_TIME_NORMAL = 4;
	const ALERT_TIME_LONG = 9;
	const SATURATION_MAX = 255;
	const SATURATION_MID = 177;
	const SATURATION_MIN = 0;
	const COLOR_GREEN = 26279;
	const COLOR_ORANGE = 8400;
	const COLOR_PINK = 59075;
	const COLOR_LILA = 50885;
	const COLOR_RED = 64475;
	const COLOR_YELLOW = 17479;
	const COLOR_WHITE = 40555;
	const COLOR_BLUE = 47110;

	protected $alert = 'none';

	protected $color = 0;

	protected $saturation = 0;

	/**
	 * @return int
	 */
	public function getColor() {
		return $this->color;
	}

	/**
	 * @param int $color
	 * @return $this
	 */
	public function setColor($color = self::COLOR_WHITE) {
		$color = (int) $color;
		if ($color < 0) {
			print 'Wrong color!';
			exit;
		}
		$this->color = $color;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getSaturation() {
		return $this->saturation;
	}

	/**
	 * @param int $saturation
	 * @return $this
	 */
	public function setSaturation($saturation = self::SATURATION_MAX) {
		$saturation = (int) $saturation;
		if ($saturation < self::SATURATION_MIN) {
			print 'Wrong saturation!';
			exit;
		}
		$this->saturation = $saturation;
		return $this;
	}

	/**
	 * Collect all state able parameters and return them.
	 *
	 * @return array
	 */
	public function getCallableStates() {
		return array(
			'on' => $this->getOn(),
			'sat' => $this->getSaturation(),
			'hue' => $this->getColor(),
			'bri' => $this->getBrightness(),
			'alert' => $this->getAlert(),
		);
	}

	/**
	 * @return string
	 */
	public function getAlert() {
		return $this->alert;
	}

	/**
	 * @param string $alert
	 * @return Light
	 */
	public function setAlert($alert) {
		if ($alert != self::ALERT_NONE && $alert != self::ALERT_ONE_BLINK && $alert != self::ALERT_BLINKING) {
			print 'Wrong alert!';
			exit;
		}
		$this->alert = $alert;
		return $this;
	}
}
?>