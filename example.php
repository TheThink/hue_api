<?php
/**
 * Copyright: 2016 - David Rühr <csw.ruehr@googlemail.com>
 * Hue Api Example
 *
 * INFO:
 * You have to set your bridge IP and your bridge key in HueApi.php
 */
$baseDir = __DIR__ . '/';
require_once $baseDir . 'HueApi.php';

// INFO: The constructor will ping your bridge. So you have to make sure that the device is ping able!
$hueApi = new HueApi;

// You will know the names of your lights to call them. This is a very easy variant to work with the lights.
$livingroomLight = $hueApi->getLight('Color Arbeitszimmer');

// Check if light exists.
if (!$livingroomLight) {
	print 'missing light'; exit;
}

// Switch light OFF if ON.
if ($livingroomLight->getOn()) {
	$livingroomLight->setOn(FALSE);
	$hueApi->putState($livingroomLight);
}

// Set the brightness to max.
$livingroomLight->setBrightness($livingroomLight::BRIGHTNESS_MAX);

// Maybe the light should show an alert?`Otherwise comment out the following light.
$livingroomLight->setAlert($livingroomLight::ALERT_BLINKING);

// Now set the color of your iris and the saturation of the selected color.
$livingroomLight->setColor($livingroomLight::COLOR_ORANGE)->setSaturation($livingroomLight::SATURATION_MAX);

// Remember, the light is still OFF. Here your will set the state to ON but you have to put the stuff above to the bridge.
if (!$livingroomLight->getOn()) {
	$livingroomLight->setOn(TRUE);
}

// Now send all the stuff to the bridge.
$hueApi->putState($livingroomLight);

// You can test the alert. Otherwise set the alert to OFF.
$alert = TRUE;
if ($alert) {
	$hueApi->alertLoop($livingroomLight, $livingroomLight::ALERT_TIME_NORMAL);
}
?>