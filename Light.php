<?php
/**
 * Copyright: 2016 - David Rühr <csw.ruehr@googlemail.com>
 * Hue Light
 */

/**
 * Class Light
 */
abstract class Light {
	const BRIGHTNESS_MAX = 254;
	const BRIGHTNESS_MID = 177;
	const BRIGHTNESS_DIMMED = 89;
	const BRIGHTNESS_MIN = 0;

	protected $id = 0;

	protected $type = '';

	protected $name = '';

	protected $on = FALSE;

	protected $reachable = FALSE;

	protected $brightness = 0;

	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param int $id
	 * @return Light
	 */
	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function getType() {
		return $this->type;
	}

	public function setType($type) {
		$this->type = $type;
		return $this;
	}

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	public function getOn() {
		return $this->on;
	}

	public function setOn($stateToSet) {
		$stateToSet = (boolean) $stateToSet;
		$this->on = $stateToSet;
		return $this;
	}

	public function getReachable() {
		return $this->reachable;
	}

	public function setReachable($stateToSet) {
		$stateToSet = (boolean) $stateToSet;
		$this->reachable = $stateToSet;
		return $this;
	}

	public function getBrightness() {
		return $this->brightness;
	}

	public function setBrightness($brightness = self::BRIGHTNESS_MIN) {
		$brightness = (int) $brightness;
		if ($brightness < self::BRIGHTNESS_MIN || $brightness > self::BRIGHTNESS_MAX) {
			exit;
		}
		$this->brightness = $brightness;
		return $this;
	}
}
?>