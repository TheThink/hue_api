# HUE API #

Eine PHP - API für Philips HUE Color Lampen (weitere Geräte folgen). Die API macht sich zu nutzen, das man den Lampen einen eigenen Namen geben kann. In der example.php findet Ihr alles nötige zum Einbinden und Ansprechen der Lampen.

### Für wen ist die API geeignet? ###

* PHP Entwickler welche die Möglichkeiten der Bridge Schnittstelle kennen lernen und schnell testen wollen 
* PHP Entwickler die in den API Bereich gerade einsteigen. Die API ist wirklich sehr einfach gehalten
* Entwickler die eine solide API für Ihre HUE Projekt nutzen möchten -> Gerne auch in Zusammenarbeit

### Wie fange ich an? ###

* Bridge IP in der HueApi.php eintragen
* Bridge Key in der HueApi.php eintragen
* Sicherstellen, das die Bridge Pingbar ist
* In der example.php in Zeile 16 den Namen der gewünschten Lampe eintragen (ersetze: Color Arbeitszimmer)
* example.php im Browser aufrufen