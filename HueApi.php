<?php
/**
 * Copyright: 2016 - David Rühr <csw.ruehr@googlemail.com>
 * Hue Api
 */
$baseDir = __DIR__ . '/';
require_once $baseDir . 'ColorLight.php';

/**
 * Class hueApi
 */
class HueApi {
	private $bridgeIp = 'YOUR_BRIDGE_IP';
	private $cryptedUserName = 'YOUR_BRIDGE_KEY';
	protected $lights = array();

	/**
	 * HueApi constructor.
	 */
	public function __construct() {
		if(!$this->deviceExists(array($this->bridgeIp))) {
			print 'Bride does not exists!';
			return;
		}
		$this->loadLights();
	}

	/**
	 * @return string
	 */
	public function getApiUrl() {
		return 'http://' . $this->bridgeIp . '/api/' . $this->cryptedUserName;
	}

	/**
	 * Check if device with IP Address exists.
	 *
	 * @param $ips
	 * @return bool
	 */
	public function deviceExists($ips) {
		if (count($ips) == 0) {
			print 'Device not found!';
			exit;
		}
		$isResponding = FALSE;
		foreach ($ips as $ip) {
			exec(sprintf('ping -c 1 -W 5 %s', escapeshellarg($ip)), $res, $returnVal);
			if ($returnVal === 0) {
				$isResponding = TRUE;
				break;
			}
			$isResponding = FALSE;
		}
		return $isResponding;
	}

	/**
	 * Load and save all existing lights in a variable.
	 *
	 * @return $this
	 */
	public function loadLights() {
		$lights = $this->getCall(
			$this->getApiUrl() . '/lights/'
		);
		if (!$lights || empty($lights)) {
			print 'Can not find lights';
			exit;
		}
		foreach ($lights as $lightId => $light) {
			if ($light['type'] == 'Color light') {
				$this->lights[$light['name']] = new ColorLight;
				$this->lights[$light['name']]
					->setId($lightId)
					->setType($light['type'])
					->setName($light['name'])
					->setOn($light['state']['on'])
					->setReachable($light['state']['reachable'])
					->setBrightness($light['state']['bri'])
					->setColor($light['state']['hue'])
					->setSaturation($light['state']['sat']);
			}
		}
		return $this;
	}

	/**
	 * Get a light instance.
	 *
	 * @param string $lightName
	 * @return mixed
	 */
	public function getLight($lightName = '') {
		if ($lightName == '' || !isset($this->lights[$lightName])) {
			return FALSE;
		}
		$light = $this->lights[$lightName];
		if ($light->getReachable()) {
			return $light;
		}
		return FALSE;
	}

	/**
	 * Execute a PUT call and set new light states.
	 *
	 * @param $lightId
	 * @param $state
	 */
	protected function executePutState($lightId, $state) {
		$dataString = json_encode($state);
		$ch = curl_init($this->getApiUrl() . '/lights/' . $lightId . '/state');
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($dataString))
		);
		$result = curl_exec($ch);
		unset($result);
	}

	/**
	 * Execute a GET call and return the light state.
	 *
	 * @param $urlToCall
	 * @return mixed
	 */
	protected function getCall($urlToCall) {
		$ch = curl_init($urlToCall);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		return json_decode(curl_exec($ch), TRUE);
	}

	/**
	 * Create the new put state data and call the execute method.
	 *
	 * @param Light $light
	 */
	public function putState(Light $light) {
		$lightId = $light->getId();
		$state = $light->getCallableStates();
		$this->executePutState($lightId, $state);
	}

	/**
	 * Starts an alert loop by recalling the alert state.
	 * @important: Remember, there is a sleep call in this method.
	 *
	 * @param Light $light
	 * @param int $countOfAlert
	 */
	public function alertLoop(Light $light, $countOfAlert = 4) {
		for ($count = 0; $count <= $countOfAlert; $count++) {
			sleep(17);
			$this->putState($light);
		}
		sleep(17);
		$light->setAlert($light::ALERT_NONE);
		$this->putState($light);
	}
}
?>